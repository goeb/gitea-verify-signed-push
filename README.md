Git hook to verify push signatures (for Gitea)
========================================================================

Requirements
------------------------------------------------------------------------

- *Gitea* using a *PostgreSQL* database
- access to the `psql` command line tool
- *GPG* with `trust-model tofu` support
- access to the `gpg` command line tool
- access to the `jq` command line tool

**Note:** This has only been tested using *Gitea*'s internal SSH server.
Using *OpenSSH* (or another SSH server) may or may not work, the same
applies to pushes over HTTP(S)!

Usage
------------------------------------------------------------------------

`verify-push-signature` is implemented as a *Git* `pre-receive` hook. To
automatically copy the hook to every new repository, the *Git* option
`[init].templateDir` may be used. Note that *Gitea* already creates its
own hook setup, the `verify-push-signature` script should be placed in
the `hooks/pre-receive.d/` directory. This also works if *Git* hooks are
disabled in *Gitea* (setting `[security].DISABLE_GIT_HOOKS` to `true`,
the default). For existing repositories, the hook has to be copied
manually. Note that by using *Git*'s `[init].templateDir` option, the
hook will also be copied to *Gitea*'s wiki repositories!

> **Tip:** Putting a symlink to the hook (which is located somewhere
  outside the `templateDir`) in the template directory will make updates
  easier.

Push signatures have to be enabled on the server, by setting the *Git*
option `[receive].certNonceSeed` to some random string.

The script accesses *Gitea*'s database directly. It is recommended to
create a separate role with read-only (`SELECT`) access to the tables
`email_address`, `gpg_key` and `gpg_key_import` only.

The script will source its configuration file from
`$HOME/.config/verify-push-signature.conf` (depending on your *Gitea*
version, `$HOME` may not be where you think it is, check the
documentation). Copy the included example `verify-push-signature.conf`
and modify it according to your needs.

In the *GnuPG* configuration (`gpg.conf` in the `$GNUPGHOME`, which by
default is `$HOME/.gnupg`), set the `trust-model tofu` option.

On the client side, *GPG* has to be set up for the repository and push
certificate signatures have to be enabled by setting the *Git* option
`[push].gpgSign` to `if-asked` (or `true`, to unconditionally sign
pushes even if the server does not ask for it).

To pass the verification, the following conditions must apply:

- an email address of the *GPG* key has to be added to the *Gitea*
  account
- this email address has to be validated
- the *GPG* public key for that email address has to be added to *Gitea*
  (after the email address has been validated!)
- this *GPG* key has to be verified
- the *GPG* key must not have expired
- the *GPG* key must not contain a UID associated with a different
  *Gitea* user

Note that the first push will always fail, at this point the script will
import the pushing user's public key - if available - into the keyring.
Following pushes should succeed.

Copyright and license
------------------------------------------------------------------------

Copyright © 2022 Stefan Göbel < gvpush ʇɐ subtype ˙ de >

The verify-push-signature script is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

The verify-push-signature script is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this script. If not, see <http://www.gnu.org/licenses/>.

`ignore:indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=72:`
